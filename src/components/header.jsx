import React, { Component } from 'react';

export default class Header extends Component{

  constructor(){
    super();
    this.state={
      showOption: false,
      folderName: ''
    };

    this.inputRef = React.createRef();

    this.showOpt = this.showOpt.bind(this);
    this.createFolder = this.createFolder.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
  }

  onInputChange(e){
    this.setState({folderName: e.target.value});
  }

  showOpt(){
    this.setState((state)=> {
      return {
        showOption: !state.showOption,
        folderName: ''
      }
    }, ()=>{
      this.inputRef.current
      ? this.inputRef.current.focus()
      : ''
    });

  }

  createFolder(){
    if(!this.state.folderName){
      alert('Please enter a valid name');
      return;
    }
    this.setState({showOption: false, folderName: ''});
    this.props.createFolder(this.state.folderName);
  }


  render(){
    return(
      <header>
        <button onClick={this.props.goBack}>&#8592;</button>
        <h3>Directory</h3>
        <div className="add-folder">
          <span onClick={this.showOpt}>Add Folder</span>
          {
            this.state.showOption
            ? <div className="option">
                <div className="folder-input">
                  <span>Folder Name</span>
                  <input type="text" val={this.state.folderName} onChange={this.onInputChange} ref={this.inputRef}/>
                </div>
                <div className="folder-action">
                  <span onClick={this.showOpt}>Cancel</span>
                  <span onClick={this.createFolder}>Create</span>
                </div>
              </div>
            : ''
          }

        </div>
      </header>
    )
  }
}
