import React, { Component } from 'react';
import { folderImage } from '../../public/images/icon';

export default class Folder extends Component{

   naviageDirectory = () =>{
    this.props.naviageDirectory(this.props.id);
  }

  render(){
    return(
      <div onClick={this.naviageDirectory}>
        <div dangerouslySetInnerHTML={{__html: folderImage}}></div>
        <div className="folder-name">{this.props.name}</div>
      </div>
    )
  }
}
