import React, { Component } from 'react';

export default class Nav extends Component{

  navigate = ()=>{
    this.props.naviageDirectory(this.props.id);
  }

  render(){
    return(
      <div onClick={this.navigate}>
        <span>{this.props.name}</span>
      </div>
    )
  }
}
