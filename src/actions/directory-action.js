import dispatcher from "../dispatcher";

export function addDirectoryAction(dirObj){
  dispatcher.dispatch({
    type: 'ADD_DIRECTORY',
    dirObj
  })
};

export function naviageDirectoryAction(id){
  dispatcher.dispatch({
    type: 'NAVIGATE_DIRECTORY',
    id
  })
}
