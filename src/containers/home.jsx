import React, { Component } from 'react';
import Header from '../components/header';
import Nav from '../components/nav';
import Folder from '../components/folder';
import DirectoryStore from '../stores/directory-store';
import { addDirectoryAction, naviageDirectoryAction } from '../actions/directory-action';

export default class Home extends Component {

  constructor(){
    super();

    let presentDirectory = DirectoryStore.getPresentDirectory();
    this.state={
      directoryMap: DirectoryStore.getAllDirectory(),
      presentDirectory,
    };

    this.setCurrentId(presentDirectory);
    this.goBack = this.goBack.bind(this);
    this.createFolder = this.createFolder.bind(this);
    this.naviageDirectory = this.naviageDirectory.bind(this);
    DirectoryStore.on('change', this.getStoreValues.bind(this));
  }

  getStoreValues(){
    let presentDirectory = DirectoryStore.getPresentDirectory();
    this.setCurrentId(presentDirectory);
    this.setState({
      directoryMap: DirectoryStore.getAllDirectory(),
      presentDirectory
    });
  }

  setCurrentId(presentDirectory){
    let lastEleObj = presentDirectory[presentDirectory.length - 1];
    this.currentFolderId = lastEleObj.id;
  }

  componentWillUnmount(){
    DirectoryStore.removeListener("change", this.getStoreValues);
  }

  createFolder(name){
    let obj = {
      name,
      parentId: this.currentFolderId
    }
    addDirectoryAction(obj);
  }

  naviageDirectory(id){
    if(id === this.currentFolderId){
      return;
    }
    naviageDirectoryAction(id)
  }

  goBack(){
    const { presentDirectory } =  this.state;
    if(presentDirectory.length > 1){
      let lastId = presentDirectory[presentDirectory.length - 2].id;
      naviageDirectoryAction(lastId);
    }
  }

  render(){
    const { directoryMap, presentDirectory } = this.state;
    let childFolder = directoryMap[this.currentFolderId].childFolder;

    return(
      <React.Fragment>
        <Header goBack={this.goBack}  createFolder={this.createFolder}/>
        <nav>
          {
            presentDirectory.map((obj)=>{
              return <Nav key={obj.id} name={obj.name} id={obj.id}
                naviageDirectory={this.naviageDirectory} />
            })
          }
        </nav>

        <section>
          {
            childFolder.length
            ? childFolder.map((key) =>{
                return <Folder key={key} name={directoryMap[key].name}
                  id={key} naviageDirectory={this.naviageDirectory} />
              })
            : 'So Empty Right Now!!!'
          }
        </section>

      </React.Fragment>
    )
  }

}
