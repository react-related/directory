import { EventEmitter } from "events";
import dispatcher from "../dispatcher";

class DirectoryStore extends EventEmitter{

  constructor(){
    super();

    let id = this.idGenerator();
    this.directory = JSON.parse(localStorage.getItem('directory'))
          || {[id]: {name: '/', childFolder: [], parentId: null}};
    this.presentFolder = JSON.parse(localStorage.getItem('presentFolder'))
          || [{id, name: '/'}];
  }

  idGenerator(){
    return Math.random().toString(36).substr(2, 4);
  }


  addDirectory(dirObj){
    let id = this.idGenerator();
    dirObj.childFolder = [];
    this.addToParent(dirObj.parentId, id);
    this.directory[id] = dirObj;
    localStorage.setItem('directory', JSON.stringify(this.directory));
    this.emit('change');
  }

  addToParent(parentId, id){
    this.directory[parentId].childFolder.push(id);
  }

  navigateDirectory(id){
    let navIndex = this.getIndex(id);
    if(!isNaN(navIndex)){
      this.presentFolder.splice(navIndex);
    }else{
      let name = this.directory[id].name;
      this.presentFolder.push({id, name});
    }
    localStorage.setItem('presentFolder', JSON.stringify(this.presentFolder));
    this.emit('change');
  }

  getIndex(id){
    let navIndex = 'i';
    this.presentFolder.map((obj, index)=>{
      if(obj.id === id){
        navIndex = index+1;
        return;
      }
    });
    return navIndex;
  }

  getPresentDirectory(){
    return this.presentFolder;
  }

  getAllDirectory(){
    return this.directory;
  }

  handleActions(action){
    switch (action.type) {
      case 'ADD_DIRECTORY':
        this.addDirectory(action.dirObj);
        break;
      case 'NAVIGATE_DIRECTORY':
        this.navigateDirectory(action.id);
        break;
      default:
    }
  }
}

const directoryStore = new DirectoryStore;
dispatcher.register(directoryStore.handleActions.bind(directoryStore));
export default directoryStore;
