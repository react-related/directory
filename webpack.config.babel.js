// We are using node's native package 'path' https://nodejs.org/api/path.html
import path from 'path';

import HtmlWebpackPlugin from 'html-webpack-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';

// Constant with our paths
const paths = {
  DIST: path.resolve(__dirname, 'dist'),
  SRC: path.resolve(__dirname, 'src'),
  PUBLIC: path.resolve(__dirname, 'public')
};

// Webpack configuration
module.exports = {
  entry: path.join(paths.SRC, 'client.js'),
  output: {
    path: paths.DIST,
    filename: 'app.bundle.js',
    publicPath: '/'
  },
  // Tell webpack to use html plugin
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(paths.PUBLIC, 'index.html')
    }),
    new ExtractTextPlugin('style.bundle.css'), // CSS will be extracted to this bundle file -> ADDED IN THIS STEP
  ],
  // Loaders configuration We are telling webpack to use "babel-loader" for .js
  // and .jsx files
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
      // CSS loader for CSS files Files will get handled by css loader and then passed
      // to the extract text plugin which will write it to the file we defined above
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract({use: 'css-loader'})
      },
      { // sass / scss loader for webpack
        test: /\.(sass|scss)$/,
        loader: ExtractTextPlugin.extract(['css-loader', 'sass-loader'])
      }
    ]
  },
  // Enable importing JS files without specifying their's extenstion
  //
  // So we can write: import MyComponent from './my-component';
  //
  // Instead of: import MyComponent from './my-component.jsx';
  resolve: {
    extensions: ['.js', '.jsx']
  },
  devServer: {
    proxy: {
      '/**': { //catch all requests
        target: '/index.html', //default target
        secure: false,
        bypass: function (req, res, opt) {
          if (req.headers.accept.indexOf('html') !== -1) {
            return '/index.html';
          }
        }
      }
    },
    port: 3030
  }
};
